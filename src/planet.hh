#ifndef PLANET_HH
#define PLANET_HH

#include "model.hh"


class Planet: public Model {

public:

    Planet(VertexVec &vertices,
          const glm::vec3 &pos=glm::vec3(),
          const glm::vec3 &rot=glm::vec3(),
          const glm::vec3 &scale=glm::vec3(1.0f, 1.0f, 1.0f),
          const unsigned int tex=0);

    Planet(VertexVec &vertices,
          std::vector<unsigned int> &indices,
          const glm::vec3 &pos=glm::vec3(),
          const glm::vec3 &rot=glm::vec3(),
          const glm::vec3 &scale=glm::vec3(1.0f, 1.0f, 1.0f),
          const unsigned int tex=0);

    virtual ~Planet();

    virtual void draw(Shader* shader, Camera* cam);
    virtual void update(float deltaTime);

    void setOrbitalVelocity(glm::vec3 &v);
    void setDistance(float d);
    void setParent(Model* parent);


private:

    float mDistance;
    float mRotOffSet;
    glm::vec3 mOrbVel;
    Model* mParent;


};

#endif // PLANET_HH
