#include "celestialfactory.hh"

CelestialFactory* CelestialFactory::mInstance = nullptr;

CelestialFactory::CelestialFactory():
    mNumCels(0) {
}


CelestialFactory* CelestialFactory::getInstance() {

    if (mInstance == nullptr) {
        mInstance = new CelestialFactory;
    }

    return mInstance;
}


Model* CelestialFactory::getCelestial(CelType type, VertexVec &v, IndexVec &i, 
                                      Model* parent, unsigned int tex) {

    ++mNumCels;
    // Model* celestial = nullptr;

    if (v.size() == 0 && i.size() == 0) {
        std::cerr << "No vertices or indices found, generating...\n";
        ShapeGenerator::getInstance()->genShape(v, i, 1.0f, 20, 20);
        std::cerr << "Generated\n";

        
    }

    if (type == sun) {

        glm::vec3 pos(0.0f);  // rand, 0.0f is the Sun
        glm::vec3 rot(0.0f);
        glm::vec3 size(1.0f);

        Sun* celestial = new Sun(v, i, pos, rot, size, tex);
        return celestial;

    } else if (type == planet) {

        float x = -5.0f + rand()%11;
        float y = -5.0f + rand()%11;
        float z = -5.0f + rand()%11;

        glm::vec3 vel(x, y, z);
        glm::vec3 pos(0.0f);
        glm::vec3 rot(0.0f);
        glm::vec3 size(1.0f);

        Planet* celestial = new Planet(v, i, pos, rot, size, tex);

        celestial->setParent(parent);
        celestial->setOrbitalVelocity(vel);
        celestial->setDistance( (rand()%10 )*6.0f + 4.0f);
        return celestial;


    } else if (type == moon) {

    }


    return nullptr;

}


Model* CelestialFactory::getCelestial(CelType type, float radius, 
                                      Model* parent, unsigned int tex) {

    VertexVec v;
    IndexVec i;
    unsigned int n = 40;
    unsigned int m = 10;
    float rad = 1.0f;

    // auto iter = RAD_VERTICES_RATE.find(rad);
    // if (iter != RAD_VERTICES_RATE.end()) {
    //     rad = radius;
    //     n = iter->second;
    // }
    ShapeGenerator::getInstance()->genShape(v, i, rad, n, m);
    // ShapeGenerator::getInstance()->genTexCoords(v); // TODO: texture coord gen generates another texture between begin and end of sphere
    // std::cout << type << std::endl;
    if (type == sun) {
        ShapeGenerator::getInstance()->genTexCoords(v, n, m);
        std::cout << "sun\n";
        
    }
    // for (unsigned int i=0; i<v.size(); ++i) {
    //     std::cerr << "texCoords: " << v[i]->tex.x << ", " << v[i]->tex.y << std::endl;
    // }

    return getCelestial(type, v, i, parent, tex);
}


unsigned int CelestialFactory::getNumCelestials() {
    return mNumCels;
}
