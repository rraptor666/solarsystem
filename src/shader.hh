#ifndef SHADER_HH
#define SHADER_HH

#include "camera.hh"
#include <fstream>
#include <GL/glew.h>
#include <iostream>
#include <string>
#include <vector>


const unsigned int SHADER_COUNT = 2;


class Shader {

public:

    Shader(const std::string &dir,
           const std::string &fsPath,
           const std::string &vsPath);

    ~Shader();

    void init();
    void bind();
    void setPerspective(Camera* cam, glm::mat4 model, bool addViewPos);
    void setSkyboxPerspective(Camera* cam, glm::mat4 model, bool addViewPos);

    void setMat4(const std::string &name, const glm::mat4 &mat);
    void setVec3(const std::string &name, const glm::vec3 &vec);



private:

    unsigned int mShaders[SHADER_COUNT];
    unsigned int mProgram;



};

#endif // SHADER_HH
