#include "planet.hh"

Planet::Planet(VertexVec &vertices,
               const glm::vec3 &pos,
               const glm::vec3 &rot,
               const glm::vec3 &scale,
               const unsigned int tex):

    Model(vertices, pos, rot, scale, tex),
    mDistance( sqrt(mPos.x*mPos.x + mPos.y*mPos.y + mPos.z*mPos.z) ),
    mRotOffSet(0.0f),
    mOrbVel(1.0f),
    mParent(nullptr) {


}

Planet::Planet(VertexVec &vertices,
               std::vector<unsigned int> &indices,
               const glm::vec3 &pos,
               const glm::vec3 &rot,
               const glm::vec3 &scale,
               const unsigned int tex):
    
    Model(vertices, indices, pos, rot, scale, tex),
    mDistance( sqrt(mPos.x*mPos.x + mPos.y*mPos.y + mPos.z*mPos.z) ),
    mRotOffSet(0.0f),
    mOrbVel(1.0f),
    mParent(nullptr) {

        // std::cout << pos_.x << ", " << pos_.y << ", " << pos_.z << std::endl;
}


Planet::~Planet() {
    
}

#include <iostream>
void Planet::draw(Shader* shader, Camera* cam) {

    shader->bind();
    shader->setPerspective(cam, getModel(), false);

    if (mTexture != 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, mTexture);
    }

    // std::cerr << meshes_.size() << std::endl;
    for (unsigned int i=0; i<mMeshes.size(); ++i) {
        // std::cerr << "draw\n";
        mMeshes[i]->draw();

    }

    // deactivate texture
    if (mTexture != 0) {
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}


void Planet::update(float deltaTime) {

    float angle = SDL_GetTicks()*0.0001f;
    
    glm::vec3 pPos(0.0f);
    if (mParent != nullptr) {
        pPos = mParent->getPos();
    }

    mPos.x = pPos.x + mDistance * cos(angle*mOrbVel.x);
    // mPos.y = mPos.y + mDistance * cos(angle*mOrbVel.y+3.141f/2.0f);
    mPos.z = pPos.z + mDistance * sin(angle*mOrbVel.x);
    
    /* Wikipedia:
    mPos.x = mPos.x*cos(angle) - mPos.z*sin(angle);
    mPos.z = mPos.x*sin(angle) + mPos.z*cos(angle);
    */

}


void Planet::setOrbitalVelocity(glm::vec3 &v) {
    mOrbVel = v;
}


void Planet::setDistance(float d) {
    mDistance = d;
}


void Planet::setParent(Model* parent) {
    mParent = parent;
}

