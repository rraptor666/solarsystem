#include "camera.hh"


Camera::Camera(const glm::vec3 &pos, 
              float field, float aspect, 
              float z_near, float z_far,
              Acceleration* accel):

    mPerspective(glm::perspective(field, aspect, z_near, z_far)),
    mCoord( new CamCoord({pos, glm::vec3(0,0,1), glm::vec3(0,1,0)}) ),
    mPitch(0.0f),
    mYaw(90.0f),
    mAccel(accel),
    mLastX(W_WIDTH/2.0f),
    mLastY(W_HEIGHT/2.0f) {
    
}

Camera::~Camera() {
}


glm::mat4 Camera::getViewProjection() {
    return mPerspective * getView();
}


glm::mat4 Camera::getView() {
    return glm::lookAt(mCoord->pos, mCoord->pos + mCoord->front, mCoord->up);
}


glm::mat4 Camera::getProjection() {
    return mPerspective;
}


glm::vec3 Camera::getPos() {
    return mCoord->pos;
}


glm::vec3 Camera::getFront() const {
    return mCoord->front;
}


glm::vec3 Camera::getUp() const {
    return mCoord->up;
}


void Camera::updateView(float &deltaTime) {

    float camSpeed = 0.002f * deltaTime;

    // update camera movement
    mCoord->pos +=  mAccel->forward * camSpeed * mCoord->front;
    mCoord->pos +=  mAccel->strafe * glm::normalize( glm::cross(mCoord->front, mCoord->up) ) * camSpeed;
    mCoord->pos.y += mAccel->ascend * camSpeed;

    // update camera rotation
    mPitch += mAccel->pitch;
    mYaw += mAccel->yaw;

    // prevents camera going upside down
    if (mPitch > 89.0f) {
        mPitch = 89.0f;
    }

    if (mPitch < -89.0f) {
        mPitch = -89.0f;
    }

    // update view
    mCoord->front.x = cos(glm::radians(mPitch)) * cos(glm::radians(mYaw));
    mCoord->front.y = sin(glm::radians(mPitch));
    mCoord->front.z = cos(glm::radians(mPitch)) * sin(glm::radians(mYaw));

}


