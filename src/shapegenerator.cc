#include "shapegenerator.hh"

ShapeGenerator* ShapeGenerator::mInstance = nullptr;

ShapeGenerator* ShapeGenerator::getInstance() {

    if (mInstance == nullptr) {
        mInstance = new ShapeGenerator;
    }

    return mInstance;
}

ShapeGenerator::ShapeGenerator() {

}

// TODO: ShapeGenerator::genTexCoords: https://en.wikipedia.org/wiki/UV_mapping
void ShapeGenerator::genShape(VertexVec &vertices, 
                              IndexVec &indices,
                              const float radius, 
                              const unsigned int rings, 
                              const unsigned int triangles) {


    const unsigned int ringsCorrected = rings/2;

    genVertices(vertices, radius, ringsCorrected, triangles);
    genIndices(indices, ringsCorrected, triangles);   
}


void ShapeGenerator::genCube(VertexVec &v) {

    v = {
        // back
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec2(0.0f, 0.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f, -0.5f),  glm::vec2(1.0f, 0.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f, -0.5f),  glm::vec2(1.0f, 1.0f), {}}),

        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f, -0.5f),  glm::vec2(1.0f, 1.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f, -0.5f),  glm::vec2(0.0f, 1.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec2(0.0f, 0.0f), {}}),

        // front
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f,  0.5f),  glm::vec2(0.0f, 0.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f,  0.5f),  glm::vec2(1.0f, 0.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f,  0.5f),  glm::vec2(1.0f, 1.0f), {}}),

        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f,  0.5f),  glm::vec2(1.0f, 1.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f,  0.5f),  glm::vec2(0.0f, 1.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f,  0.5f),  glm::vec2(0.0f, 0.0f), {}}),

        // right
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f,  0.5f),  glm::vec2(0.0f, 0.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f, -0.5f),  glm::vec2(1.0f, 0.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec2(1.0f, 1.0f), {}}),

        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec2(1.0f, 1.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f,  0.5f),  glm::vec2(0.0f, 1.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f,  0.5f),  glm::vec2(0.0f, 0.0f), {}}),

        // left
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f,  0.5f),  glm::vec2(0.0f, 0.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f, -0.5f),  glm::vec2(1.0f, 0.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f, -0.5f),  glm::vec2(1.0f, 1.0f), {}}),

        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f, -0.5f),  glm::vec2(1.0f, 1.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f,  0.5f),  glm::vec2(0.0f, 1.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f,  0.5f),  glm::vec2(0.0f, 0.0f), {}}),

        // bottom
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec2(0.0f, 0.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f, -0.5f),  glm::vec2(1.0f, 0.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f,  0.5f),  glm::vec2(1.0f, 1.0f), {}}),

        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f,  0.5f),  glm::vec2(1.0f, 1.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f,  0.5f),  glm::vec2(0.0f, 1.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec2(0.0f, 0.0f), {}}),

        // top
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f, -0.5f),  glm::vec2(0.0f, 0.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f, -0.5f),  glm::vec2(1.0f, 0.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f,  0.5f),  glm::vec2(1.0f, 1.0f), {}}),

        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f,  0.5f),  glm::vec2(1.0f, 1.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f,  0.5f),  glm::vec2(0.0f, 1.0f), {}}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f, -0.5f),  glm::vec2(0.0f, 0.0f), {}})

    };
}


// TODO: design algorithm for generating cube vertices and indices
void ShapeGenerator::genCube(VertexVec &v, IndexVec &i) {

   // front
    v.push_back( std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f,  0.5f), {}, {}}) ); // 0 0
    v.push_back( std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f,  0.5f), {}, {}}) ); // 1 0
    v.push_back( std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f,  0.5f), {}, {}}) ); // 1 1
    v.push_back( std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f,  0.5f), {}, {}}) ); // 0 1

    // back
    v.push_back( std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f, -0.5f), {}, {}}) ); // 0 1
    v.push_back( std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f, -0.5f), {}, {}}) ); // 0 0
    v.push_back( std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f, -0.5f), {}, {}}) ); // 1 0
    v.push_back( std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f, -0.5f), {}, {}}) ); // 1 1
    
    i = {

        // front
        0, 1, 2,
        2, 3, 0, 

        // left
        4, 0, 3, 
        3, 7, 4,

        // back
        5, 4, 7, 
        7, 6, 5,

        // right
        1, 5, 6, 
        6, 2, 1, 

        // top
        3, 2, 6, 
        6, 7, 3, 

        // bottom
        4, 5, 1, 
        1, 0, 4
    };


}


void ShapeGenerator::genTexCoords(TexVec &texCoords, VertexVec &vertices) {

    for (unsigned int i=0; i<vertices.size(); ++i) {

         double u = 0.5f + glm::atan(vertices[i]->pos.z, vertices[i]->pos.x)/(2*PI);

         double v = 0.5f - glm::asin(vertices[i]->pos.y)/(2*PI);

         texCoords.push_back({u, v});
    }

}


// TODO: fix, https://stackoverflow.com/questions/14014524/what-is-the-correct-way-to-map-a-texture-on-a-sphere

// BUG FIX: last of triangles -loop doesnt't connect to loops first u and v (from second last to first must be exactly firsts coords)
void ShapeGenerator::genTexCoords(VertexVec &vertices) {

    for (unsigned int i=0; i<vertices.size(); ++i) {

         /*
        double u = 0.5f + glm::atan(vertices[i]->pos.z, vertices[i]->pos.x)/(2*PI);

        double v = 0.5f - glm::asin(vertices[i]->pos.y)/(2*PI);
         */

        // TODO: this but not for last loop of rings but for last loop of triangles
        // if (i == vertices.size()-1) {
        //     double u_0 = vertices[0]->tex.x;
        //     double v_0 = vertices[0]->tex.y;
        //     vertices[i]->tex = {u_0, v_0};
        //     break;
        // }

        // glm::vec3 n = vertices[i]->pos; 
        // n = glm::normalize(n);
        // double u = atan2(n.x, n.z) / (2*PI) + 0.5;
        // double v = n.y * 0.5 + 0.5;

        double u = 0.5f + glm::atan(vertices[i]->pos.z, vertices[i]->pos.x)/(2*PI);
        double v = 0.5f - glm::asin(vertices[i]->pos.y)/(2*PI);

         vertices[i]->tex = {u, v};
    }
}

void ShapeGenerator::genTexCoords(VertexVec &vertices, unsigned int rings, unsigned int triangles) {

    std::cout << "Vertices: " << vertices.size() << std::endl;
    for (unsigned int i=1; i<vertices.size(); ++i) {

        //TODO: try with more vertices
        // if ( i != 0 && (i+1)%triangles == 0 ) {

        //     double u_0 = 0.0f;
        //     double v_0 = 0.0f;
        //     if (i < triangles) {
        //         u_0 = vertices[i-1]->tex.x;
        //         v_0 = vertices[i-1]->tex.y;
            
        //     } else {
        //         u_0 = vertices[i-triangles+1]->tex.x;
        //         v_0 = vertices[i-triangles+1]->tex.y;
        //     }
        //     vertices[i]->tex = {u_0, v_0};
        //     continue;
        // }

        glm::vec3 n = vertices[i]->pos; 
        n = glm::normalize(n);
        double u = atan2(n.x, n.z) / (2*PI) + 0.5;
        double v = n.y * 0.5 + 0.5;


        vertices[i]->tex = {u, v};
    }

}


void ShapeGenerator::genVertices(VertexVec &vertices, 
                                 const float radius, 
                                 const unsigned int rings, 
                                 const unsigned int triangles) {

    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;

    const float SHIFT = PI/(rings);

    float theta = -PI;  // ring start angle
    float phi = 0.0f;   // triangle start angle

    x = radius * sin(theta) * sin(phi);
    y = radius * cos(theta);
    z = radius * sin(theta) * cos(phi);

    // south pole
    vertices.push_back( std::make_shared<Vertex> (Vertex{glm::vec3(0.0f, -radius, 0.0f), {}, {}}) );
    // addTexCoord(vertices, 0);

    theta += SHIFT;

    for (unsigned int i=0; i<rings-1; ++i) {

        for (unsigned int j=0; j<triangles; ++j) {

            // calculate cartesian coordinates
            x = radius * sin(theta) * sin(phi);
            y = radius * cos(theta);
            z = radius * sin(theta) * cos(phi);

            vertices.push_back( std::make_shared<Vertex> (Vertex{glm::vec3(x, y, z), {}, {}}) );
            // addTexCoord(vertices, vertices.size()-1);

            // update angle
            phi += 2 * PI / triangles;

        }

        // update angle
        theta += SHIFT;
        phi = 0.0f;


    }

    // north pole
    vertices.push_back( std::make_shared<Vertex> (Vertex{glm::vec3(0.0f, radius, 0.0f), {}, {}}) );
    // addTexCoord(vertices, vertices.size()-1);
}


void ShapeGenerator::genIndices(IndexVec &indices, 
                                const unsigned int rings,
                                const unsigned int triangles) {

    for (unsigned int j=0; j<rings-1; ++j) {

        for (unsigned int i=0; i<triangles; ++i) {

            if (j == 0) {

                if (i == triangles - 1) {
                    indices.push_back(i+1);  // or number of triangles
                    indices.push_back(0);
                    indices.push_back(1);

                } else {
                    indices.push_back(i+1);
                    indices.push_back(j);
                    indices.push_back(i+2);    
                }

                // if (i == 1) {
                //     return;
                // }
                continue;
            }

            // 1st triangle
            if (i == triangles - 1) {
                indices.push_back( j*triangles + i + 1 );
                indices.push_back( (j-1)*triangles + i + 1 );
                indices.push_back( j*triangles + 1 );

            } else {
                indices.push_back( j*triangles + i + 1 );
                indices.push_back( (j-1)*triangles + i + 1 );
                indices.push_back( j*triangles + i + 2 );
            }

            // 2nd triangle
            if (i == triangles - 1) {
                indices.push_back( j*triangles + 1 );
                indices.push_back( j*triangles );
                indices.push_back( (j-1)*triangles + 1 );
            
            } else {
                indices.push_back( j*triangles + i + 2 );
                indices.push_back( (j-1)*triangles + i + 1 );
                indices.push_back( (j-1)*triangles + i + 2 );
            }


        }
    }

    // north pole indices
    unsigned int numVertices = 2 + (rings-1)*triangles;
    for (unsigned int i=1; i<triangles; ++i) {

        if (i != triangles) {
            indices.push_back( numVertices-triangles + i - 2);
            indices.push_back( numVertices-triangles + i - 1);
            indices.push_back( numVertices -1 );
        
        } else {

        }

        /*
        counter-clockwise:
           2
          / \
         /   \
        0-----1
        */
    }
    indices.push_back( numVertices - 2 );
    indices.push_back( numVertices-triangles - 1 );
    indices.push_back( numVertices - 1 );


}


void ShapeGenerator::genShape(VertexVec &vertices, 
                              const float radius, 
                              const unsigned int rings, 
                              const unsigned int triangles) {

    float theta = -PI;
    float phi = 0.0f;
    const float SHIFT = PI/rings;

    glm::vec3 southPole( {0.0f, -radius, 0.0f} );
    glm::vec3 northPole( {0.0f, radius, 0.0f} );



    for (unsigned int i=0; i<rings; ++i) {

        // calculate cartesian coordinates
        float x = radius * sin(theta) * sin(phi);
        float y = radius * cos(theta);
        float z = radius * sin(theta) * cos(phi);

        phi += 2 * PI/triangles;

        glm::vec3 vertex0( {x, y, z} );
        
        for (unsigned j=0; j<triangles; ++j) {

            // calculate cartesian coordinates
            x = radius * sin(theta) * sin(phi);
            y = radius * cos(theta);
            z = radius * sin(theta) * cos(phi);
            
            glm::vec3 vertex1( {x, y, z} );

            if (i == 0) {
                genSouthCap(vertices, vertex0, vertex1, southPole, j, triangles);

            } else if (i == 1) {

                /*
                2,5---4               
                |  \ 2|
                | 1 \ |
                0---1,3
                */
                if (j == 0) {

                    // first triangle
                    vertices.push_back( std::make_shared<Vertex> (Vertex{vertex0, {}, {}}) );
                    vertices.push_back( std::make_shared<Vertex> (Vertex{vertex1, {}, {}}) );
                    vertices.push_back( std::shared_ptr<Vertex> ( vertices[0] ) );

                    // second triangle
                    vertices.push_back( std::make_shared<Vertex> (Vertex{vertex1, {}, {}}) );
                    vertices.push_back( std::shared_ptr<Vertex> (vertices[1]) );
                    vertices.push_back( std::shared_ptr<Vertex> (vertices[0]) );

                }

            } else if (i < rings-1) {


            } else {
                genNorthCap(vertices, northPole, j, triangles);
            }



            phi += 2 * PI / triangles;

        }


        theta += SHIFT;
        phi = 0.0f;

    }




}

void ShapeGenerator::genSouthCap(VertexVec &vertices, 
                                 glm::vec3 &vertex0, 
                                 glm::vec3 &vertex1, 
                                 glm::vec3 &southPole,
                                 unsigned int j,
                                 const unsigned int triangles) {
    
    if (j == 0) {
        vertices.push_back( std::make_shared<Vertex> (Vertex{vertex0, {}, {}}) );
        vertices.push_back( std::make_shared<Vertex> (Vertex{vertex1, {}, {}}) );
        vertices.push_back( std::make_shared<Vertex> (Vertex{southPole, {}, {}}) );
    
    } else if (j < triangles-1) {
        vertices.push_back( vertices[vertices.size()-2] );
        vertices.push_back( std::make_shared<Vertex> (Vertex{vertex1, {}, {}}) );
        vertices.push_back( std::make_shared<Vertex> (Vertex{southPole, {}, {}}) );

    } else {
        vertices.push_back( vertices[vertices.size()-2] );
        vertices.push_back( std::make_shared<Vertex> (Vertex{vertex0, {}, {}}) );
        vertices.push_back( std::make_shared<Vertex> (Vertex{southPole, {}, {}}) );
    }

}


void ShapeGenerator::genFirstRing(VertexVec &vertices, 
                                  glm::vec3 &vertex0, 
                                  glm::vec3 &vertex1,
                                  unsigned int j,
                                  const unsigned int triangles) {

}


void ShapeGenerator::genMidRing(VertexVec &vertices, 
                                glm::vec3 &vertex0, 
                                glm::vec3 &vertex1,
                                unsigned int j,
                                const unsigned int triangles) {

}


void ShapeGenerator::genNorthCap(VertexVec &vertices, 
                                 glm::vec3 &northPole,
                                 unsigned int j,
                                 const unsigned int triangles) {

    if (j < triangles-1) {
        vertices.push_back( std::make_shared<Vertex> (Vertex{northPole, {}, {}}));
        vertices.push_back( std::shared_ptr<Vertex> (vertices[vertices.size()-1-triangles*6]) );
        vertices.push_back( std::shared_ptr<Vertex> (vertices[vertices.size()-3-triangles*6]) );

    } else {
        vertices.push_back( std::make_shared<Vertex> (Vertex{northPole, {}, {}}));
        vertices.push_back( std::shared_ptr<Vertex> (vertices[vertices.size()-2]) );
        vertices.push_back( std::shared_ptr<Vertex> (vertices[vertices.size()-3-(triangles-1)*6]) );        
    }

}


double ShapeGenerator::calculateK(double &theta, double &phi) {
    (void)theta;
    (void)phi;
    return 0.0f;
}


double ShapeGenerator::calculateX(double &theta, double &phi) {
    (void)theta;
    (void)phi;
    return 0.0f;
}


double ShapeGenerator::calculateY(double &theta, double &phi) {
    (void)theta;
    (void)phi;
    return 0.0f;
}


double ShapeGenerator::calculateTexCoord(double &x, double &z) {

//    return (sqrt(2 / (1-z)) * x);
    return (1 / (x-z));

}


std::pair<double, double> ShapeGenerator::calculateFromNormal(Vertex* vertex)
{
    return {asin(vertex->nor.x)/PI + 0.5f, asin(vertex->nor.y)/PI + 0.5f};
}


void ShapeGenerator::correctNormals(Vertex* v1, Vertex* v2, Vertex* v3) {
    // Calculate vectors
    glm::vec3 pos_1 = v1->pos;
    glm::vec3 pos_2 = v2->pos;
    glm::vec3 pos_3 = v3->pos;

    float var1_x = pos_2.x - pos_1.x;
    float var1_y = pos_2.y - pos_1.y;
    float var1_z = pos_2.z - pos_1.z;

    float var2_x = pos_3.x - pos_1.x;
    float var2_y = pos_3.y - pos_1.y;
    float var2_z = pos_3.z - pos_1.z;

    // Get cross product of vectors
    glm::vec3 normal;
    normal.x = (var1_y * var2_z) - (var2_y * var1_z);
    normal.y = (var1_z * var2_x) - (var2_z * var1_x);
    normal.z = (var1_x * var2_y) - (var2_x * var1_y);

    // Normalise final vector
    float vLen = sqrt( (normal.x * normal.x) + (normal.y * normal.y) + (normal.z * normal.z) );

    normal.x = normal.x/vLen;
    normal.y = normal.y/vLen;
    normal.z = normal.z/vLen;

    v1->nor = normal;
    v2->nor = normal;
    v3->nor = normal;
}
