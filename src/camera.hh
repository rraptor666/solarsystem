#ifndef CAMERA_HH
#define CAMERA_HH

#include "constants.hh"
#include "datastructures.hh"

#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <SDL2/SDL.h>


class Camera {

public:

    Camera(const glm::vec3 &pos, 
           float field, float aspect, 
           float z_near, float z_far,
           Acceleration* accel);
    ~Camera();
    
    glm::mat4 getViewProjection();
    glm::mat4 getView();
    glm::mat4 getProjection();

    glm::vec3 getPos();
    glm::vec3 getFront() const;
    glm::vec3 getUp() const;

    void updateView(float &deltaTime);


    
private:
    
    glm::mat4 mPerspective;
    CamCoord* mCoord;

    // Euler angles
    float mPitch;
    float mYaw;

    Acceleration* mAccel;

    float mLastX;
    float mLastY;




};


#endif
