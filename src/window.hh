#ifndef WINDOW_HH
#define WINDOW_HH

#include "constants.hh"
#include <GL/glew.h>
#include <iostream>
#include <random>
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <time.h>




class Window {

public:

    Window();
    ~Window();

    void swapBuffers();
    void clear();

    void updateFPSCounter(float &deltaTime);



private:

    bool init();

    bool mInited;
    std::string mTitle;

    SDL_Window* mWindow;
    SDL_GLContext mContext;

};

#endif // WINDOW_HH
