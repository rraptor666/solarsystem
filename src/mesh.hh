#ifndef MESH_HH
#define MESH_HH

#include "datastructures.hh"
#include "shader.hh"
#include "tiny_obj_loader.h"
#include <GL/glew.h>
#include <iostream>
#include <string>
#include <vector>

enum { POSITION_VB, TEXCOORD_VB, NORMAL_VB, INDEX_VB, BUFFER_COUNT };


class Mesh {

public:

    Mesh(VertexVec &vertices);
    Mesh(const VertexVec &vertices);
    Mesh(VertexVec &vertices, IndexVec &indices);
    Mesh(VertexVec &vertices, std::vector<tinyobj::index_t> &indices);

    Mesh(VertexVec &vertices, IndexVec &indices, TexVec &texCoords);

    ~Mesh();

    void draw();

    unsigned int getVertexCount();
    unsigned int getIndexCount();

    unsigned int getVAO();
    unsigned int getLightVAO();




private:

    void init();
    void initLightVBO();
    void initPos(std::vector<glm::vec3> &vertices);
    void initTex(std::vector<glm::vec2> &texCoords);
    void initNor(std::vector<glm::vec3> &vec);

    void initIndices(std::vector<tinyobj::index_t> &vec);
    void initIndices();

    // vertex array object
    unsigned int mVAO;
    unsigned int mLightVAO;

    // TODO: rename to mBuffers
    unsigned int mVAB[BUFFER_COUNT];    // TODO: change to uint* so buffer count can be specified later

    unsigned int mVBO;
    unsigned int mTBO;
    unsigned int mEBO;
    VertexVec mVertices;
    IndexVec mIndices;
    TexVec mTexCoords;


};

#endif // MESH_HH
