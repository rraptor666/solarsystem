#include "input.hh"

Input* Input::mInstance = nullptr;

Input* Input::getInstance() {

    if (mInstance == nullptr) {
        mInstance = new Input;
    }

    return mInstance;
}


bool Input::handleInput(SDL_Event *e) {

    while ( SDL_PollEvent(e) ) {

        const Uint8* keyStates = SDL_GetKeyboardState(NULL);

        // mouse click on "close window"-icon or "q"
        if (e->type == SDL_QUIT || keyStates[SDL_SCANCODE_Q]) {
            return false;
        }


        // forward
        if (keyStates[SDL_SCANCODE_W]) {
            mAccel->forward = 1.0f;

        // backwards
        } else if (keyStates[SDL_SCANCODE_S]) {
            mAccel->forward = -1.0f;

        } else {
            mAccel->forward = 0.0f;
        }

        // ascend
        if (keyStates[SDL_SCANCODE_SPACE] || keyStates[SDL_SCANCODE_R]) {
            mAccel->ascend = 1.0f;

        // descend
        } else if (keyStates[SDL_SCANCODE_LSHIFT] || keyStates[SDL_SCANCODE_F]) {
            mAccel->ascend = -1.0f;

        } else {
            mAccel->ascend = 0.0f;
        }

        // strafe left
        if (keyStates[SDL_SCANCODE_A]) {
            mAccel->strafe = -1.0f;

        // strafe right
        } else if (keyStates[SDL_SCANCODE_D]) {
            mAccel->strafe = 1.0f;

        } else {
            mAccel->strafe = 0.0f;
        }

        // pitch up
        if (keyStates[SDL_SCANCODE_I]) {
            mAccel->pitch = 1.0f;

        // pitch down
        } else if (keyStates[SDL_SCANCODE_K]) {
            mAccel->pitch = -1.0f;

        } else {
            mAccel->pitch = 0.0f;
        }

        // yaw left
        if (keyStates[SDL_SCANCODE_J]) {
            mAccel->yaw = -1.0f;

        // yaw right
        } else if (keyStates[SDL_SCANCODE_L]) {
            mAccel->yaw = 1.0f;

        } else {
            mAccel->yaw = 0.0f;
        }


        // super speed
        if (keyStates[SDL_SCANCODE_LCTRL]) {
            mAccel->forward *= 3.0f;
            mAccel->strafe *= 3.0f;
            mAccel->ascend *= 3.0f;
        }
    }

    return true;
}


Acceleration* Input::getAccel() {
    return mAccel;
}



Input::Input():
    mAccel( new Acceleration({0.0f, 0.0f, 0.0f, 0.0f, 0.0f}) ) {

}