#ifndef SKYBOX_HH
#define SKYBOX_HH

#include "model.hh"
#include "utility.hh"


// TODO: finish inti with normal cubemaps: shaders must be modified and
// add mInitWithNormalCubeMaps to be able to call normal Shader::setPerspective

class Skybox: public Model {

public:
    // normal cubemap loading
    Skybox(VertexVec &v, unsigned int texture);

    // cubemap loading using meshvec
    Skybox(MeshVec &m, glm::vec3 pos, std::vector<unsigned int> &textures);
    ~Skybox();

    virtual void draw(Shader* shader, Camera* cam);

private:
    bool init();

    std::vector<unsigned int> mCubeTex;

};



#endif