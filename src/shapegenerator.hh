#ifndef SHAPEGENERATOR_HH
#define SHAPEGENERATOR_HH

#include "datastructures.hh"
#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <iostream>
#include <vector>

const double PI = glm::pi<float>();
/**
 * The ShapeGenerator class, used to generate vertices for a sphere with
 * given size and number of latitudes(rings) and longitudes(triangles).
 *
 * Could also be used to generate other shaped using different parameter values.
 *
 * For information about spherical coordinate system:
 * https://en.wikipedia.org/wiki/Spherical_coordinate_system
 *
 * Shapes: rings x triangles, (ringsCorrected = rings/2) 
 * 4x4 = diamond
 * >30x30 = sphere
 * 
 *
 */
class ShapeGenerator {

public:
    static ShapeGenerator* getInstance();

    /**
     * @brief genShape, creates shape from given parameters
     * @param vec, storage for vertices
     * @param radius, distance between center vertex and outer vertices
     * @param rings, "latitudes" (more visualizing with spheres)
     * @param triangles, number of triangles per ring
     * @param vec, storage for vertices of the generated shape
     */
    void genShape(VertexVec &vertices, 
                  const float radius, 
                  const unsigned int rings, 
                  const unsigned int triangles);



    void genShape(VertexVec &vertices, 
                  IndexVec &indices,
                  const float radius, 
                  const unsigned int rings, 
                  const unsigned int triangles);

    void genCube(VertexVec &v);
    void genCube(VertexVec &v, IndexVec &i);

    void genTexCoords(TexVec &texCoords, VertexVec &vertices);
    void genTexCoords(VertexVec &vertices);
    void genTexCoords(VertexVec &vertices, unsigned int rings, unsigned int triangles);

private:
    ShapeGenerator();
    static ShapeGenerator* mInstance;

    void genVertices(VertexVec &vertices, 
                     const float radius, 
                     const unsigned int rings, 
                     const unsigned int triangles);

    void genIndices(IndexVec &indices, 
                    const unsigned int rings,
                    const unsigned int triangles);

    
    // vec3 as ref or not?
    void genSouthCap(VertexVec &vertices, 
                     glm::vec3 &vertex0, 
                     glm::vec3 &vertex1, 
                     glm::vec3 &southPole,
                     unsigned int j,
                     const unsigned int triangles);

    void genFirstRing(VertexVec &vertices, 
                      glm::vec3 &vertex0, 
                      glm::vec3 &vertex1,
                      unsigned int j,
                      const unsigned int triangles);

    void genMidRing(VertexVec &vertices, 
                    glm::vec3 &vertex0, 
                    glm::vec3 &vertex1,
                    unsigned int j,
                    const unsigned int triangles);

    void genNorthCap(VertexVec &vertices, 
                     glm::vec3 &northPole,
                     unsigned int j,
                     const unsigned int triangles);

    // Lambert azimuthal equal-area projection
    double calculateK(double &theta, double &phi);
    double calculateX(double &theta, double &phi);
    double calculateY(double &theta, double &phi);

    double calculateTexCoord(double &x, double &z);
    std::pair<double, double> calculateFromNormal(Vertex* vertex);
    void correctNormals(Vertex* v1, Vertex* v2, Vertex* v3);

};

#endif // SHAPEGENERATOR_HH
