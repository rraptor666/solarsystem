#ifndef INPUT_HH
#define INPUT_HH

#include "datastructures.hh"
#include <SDL2/SDL.h>

class Input {

public:
    static Input* getInstance();

    bool handleInput(SDL_Event *e);
    Acceleration* getAccel();


private:
    Input();

    static Input* mInstance;
    Acceleration* mAccel;

};


#endif