#include "skybox.hh"

Skybox::Skybox(VertexVec &v,unsigned int texture):
    Model(v, glm::vec3(0.0f), glm::vec3(0.0f), glm::vec3(1.0f), texture) {


}


Skybox::Skybox(MeshVec &m, glm::vec3 pos, std::vector<unsigned int> &textures):
    Model(m, pos, glm::vec3(0.0f), glm::vec3(1.0f)) {

        mCubeTex = textures;

}


Skybox::~Skybox() {

}


void Skybox::draw(Shader* shader, Camera* cam) {
    shader->bind();
    shader->setSkyboxPerspective(cam, getModel(), false);

    for (unsigned int i=0; i<mCubeTex.size(); ++i) {

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, mCubeTex[i]);

        mMeshes[i]->draw();


        glBindTexture(GL_TEXTURE_2D, 0);
    }
}