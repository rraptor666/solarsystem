#include "sun.hh"


Sun::Sun(VertexVec &vertices,
         const glm::vec3 &pos,
         const glm::vec3 &rot,
         const glm::vec3 &scale,
         const unsigned int tex):

    Model(vertices, pos, rot, scale, tex) {

}


Sun::Sun(VertexVec &vertices,
         IndexVec &indices,
         const glm::vec3 &pos,
         const glm::vec3 &rot,
         const glm::vec3 &scale,
         const unsigned int tex):

    Model(vertices, indices, pos, rot, scale, tex) {

}

Sun::~Sun() {

}


void Sun::draw(Shader *shader, Camera *cam) {

    shader->bind();
    shader->setPerspective(cam, getModel(), false);

    if (mTexture != 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, mTexture);
    }


    for (unsigned int i=0; i<mMeshes.size(); ++i) {
        mMeshes[i]->draw();

    }

    // deactivate texture
    if (mTexture != 0) {
        glBindTexture(GL_TEXTURE_2D, 0);
    }

}

void Sun::update(float deltaTime) {

}


