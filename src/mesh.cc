#include "mesh.hh"


Mesh::Mesh(VertexVec &vertices):
    mVertices(vertices) {

        init();
}


Mesh::Mesh(const VertexVec &vertices):
    mVertices(vertices ) {

    init();
}


Mesh::Mesh(VertexVec &vertices, std::vector<tinyobj::index_t> &indices):
    mVertices(vertices) {

    init();

}

Mesh::Mesh(VertexVec &vertices, std::vector<unsigned int> &indices):
    mVertices(vertices),
    mIndices(indices) {

    init();

}


Mesh::Mesh(VertexVec &vertices, IndexVec &indices, TexVec &texCoords): 
    mVertices(vertices),
    mIndices(indices),
    mTexCoords(texCoords) {

        init();
}




Mesh::~Mesh() {

    for (int i=mVertices.size()-1; i>=0; --i) {
        mVertices.pop_back();
    }

    glDeleteVertexArrays(1, &mVAO);
    glDeleteVertexArrays(1, &mLightVAO);

    glDeleteBuffers(1, &mVBO);
    glDeleteBuffers(1, &mTBO);
    glDeleteBuffers(1, &mEBO);

}

void Mesh::draw()
{
    // glBindVertexArray(mLightVAO);
    glBindVertexArray(mVAO);

    // std::cerr << indices_.size() << std::endl;
    if (mIndices.size() > 0) {
        //glDrawElements(GL_LINE_STRIP, indices_.size(), GL_UNSIGNED_INT, 0); // debugger
        glDrawElements(GL_TRIANGLES, mIndices.size(), GL_UNSIGNED_INT, 0);
        // glDrawArrays(GL_POINTS, 0, vertices_.size());
        // std::cerr << "elements\n";
    } else {
        // glDrawArrays(GL_LINES, 0, vertices_.size()); // TODO: debug vertice generator with this
        glDrawArrays(GL_TRIANGLES, 0, mVertices.size());
        // std::cerr << "arrays\n";
    }
    glBindVertexArray(0);
}


unsigned int Mesh::getVertexCount() {
    return mVertices.size();
}


unsigned int Mesh::getIndexCount() {
    return mIndices.size();
}


unsigned int Mesh::getVAO() {
    return mVAO;
}


unsigned int Mesh::getLightVAO() {
    return mLightVAO;
}


void Mesh::init() {

    std::vector<glm::vec3> positions;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texCoords;

    positions.reserve(mVertices.size());
    normals.reserve(mVertices.size());

    if (mTexCoords.size() == 0) {
        texCoords.reserve(mVertices.size());
    }


    for (unsigned int i=0; i<mVertices.size(); ++i) {
        positions.push_back( mVertices[i]->pos );
        normals.push_back( mVertices[i]->nor );

        if (mTexCoords.size() == 0) {
            texCoords.push_back( mVertices[i]->tex );
        }
    }


    glGenVertexArrays(1, &mVAO);
    glBindVertexArray(mVAO);

    glGenBuffers(1, &mVBO);
    glGenBuffers(1, &mTBO);
    glGenBuffers(1, &mEBO);

    // VBO
    initPos(positions);


    // lightVBO
    // initLightVBO();

    // textures
    if (mTexCoords.size() > 0) {
        initTex(mTexCoords);
    
    } else {
        initTex(texCoords);
    }

    if (mIndices.size() > 0) {
        initIndices();
    }
    // TODO: does this need normals?
    // normals
    // initNor(normals);

   glBindVertexArray(0);


}


void Mesh::initLightVBO() {

    glGenVertexArrays(1, &mLightVAO);
    glBindVertexArray(mLightVAO);
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);   
}


void Mesh::initPos(std::vector<glm::vec3> &vertices) {

    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vertices[0]), &vertices[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);   
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
}


void Mesh::initTex(std::vector<glm::vec2> &texCoords) {

    glBindBuffer(GL_ARRAY_BUFFER, mTBO);
    glBufferData(GL_ARRAY_BUFFER, texCoords.size() * sizeof(texCoords[0]), &texCoords[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
}


void Mesh::initNor(std::vector<glm::vec3> &vec) {
    glBindBuffer(GL_ARRAY_BUFFER, mVAB[NORMAL_VB]);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
}

void Mesh::initIndices(std::vector<tinyobj::index_t> &vec) {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mVAB[INDEX_VB]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, vec.size() * sizeof(vec[0]), &vec[0], GL_STATIC_DRAW);
}


void Mesh::initIndices() {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mIndices.size()*sizeof(mIndices[0]), &mIndices[0], GL_STATIC_DRAW);
}


