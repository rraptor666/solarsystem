#ifndef UTILITY_HH
#define UTILITY_HH

#include "datastructures.hh"
#include "mesh.hh"
#include "model.hh"

#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>


namespace utility {


/**
 * @brief split, splits string into vector of strings by "separator"
 * @param s, string to be split
 * @param separator
 * @param removeSpaces
 * @return split string
 */
std::vector<std::string> split(std::string s,
                               char separator,
                               bool removeSpaces=false);


void genMeshes(MeshVec &m, const unsigned int meshCount, VertexVec &v);


// TODO: does not give right values
void printModelStats(const std::string &objName, ModelMap &models, std::string name="");

} // utility

#endif // UTILITY_HH
