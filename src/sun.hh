#ifndef SUN_HH
#define SUN_HH

#include "model.hh"


class Sun : public Model {

public:
    Sun(VertexVec &vertices,
        const glm::vec3 &pos=glm::vec3(),
        const glm::vec3 &rot=glm::vec3(),
        const glm::vec3 &scale=glm::vec3(1.0f, 1.0f, 1.0f),
        const unsigned int tex=0);

    Sun(VertexVec &vertices,
        IndexVec &indices,
        const glm::vec3 &pos=glm::vec3(),
        const glm::vec3 &rot=glm::vec3(),
        const glm::vec3 &scale=glm::vec3(1.0f, 1.0f, 1.0f),
        const unsigned int tex=0);

    virtual ~Sun();

    virtual void draw(Shader *shader, Camera *cam);
    virtual void update(float deltaTime);

};

#endif // SUN_HH
