#ifndef MODEL_HH
#define MODEL_HH

#include "asset.hh"
#include "camera.hh"
#include "mesh.hh"
#include "shader.hh"
#include <GL/glew.h>
#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <vector>



class Model {

public:

    Model(MeshVec &meshes,
          const glm::vec3 &pos=glm::vec3(),
          const glm::vec3 &rot=glm::vec3(),
          const glm::vec3 &scale=glm::vec3(1.0f, 1.0f, 1.0f),
          const unsigned int tex=0);

    Model(const std::string &file);

    Model(VertexVec &vertices,
          const glm::vec3 &pos=glm::vec3(),
          const glm::vec3 &rot=glm::vec3(),
          const glm::vec3 &scale=glm::vec3(1.0f, 1.0f, 1.0f),
          const unsigned int tex=0);

    // with indices
    Model(VertexVec &vertices,
          std::vector<unsigned int> &indices,
          const glm::vec3 &pos=glm::vec3(),
          const glm::vec3 &rot=glm::vec3(),
          const glm::vec3 &scale=glm::vec3(1.0f, 1.0f, 1.0f),
          const unsigned int tex=0);

    virtual ~Model();

    virtual void draw(Shader* shader, Camera* cam);
    virtual void update(float deltaTime);

    glm::mat4 getModel() const;
    glm::vec3 getPos() const;
    glm::vec3 getRot() const;
    glm::vec3 getScale() const;
    MeshVec getMeshes() const;
    Stat* getStat();
//    Material getMaterial();

    void setPos(glm::vec3 &newPos);
    void setRot(glm::vec3 &newRot);
    void setScale(glm::vec3 &newScale);




protected:

    MeshVec mMeshes;
    glm::vec3 mPos;
    glm::vec3 mRot;
    glm::vec3 mScale;

    glm::vec3 mOrigPos;
    unsigned int mTexture;
//    Material mMaterial;
    Stat mStat;


private:

    void loadMeshes(const std::string &file);

    float mDistance;
    float mRotOffSet;



};

#endif // MODEL_HH
