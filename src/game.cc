#include "game.hh"


Game::Game(int argc, char** argv):

    mInited(false),
    mTime_0(SDL_GetTicks()),
    mWindow(new Window),
    mEvent(new SDL_Event),
    mGen( ShapeGenerator::getInstance() ),
    mAsset(new Asset),
    mInput( Input::getInstance() ),
    mCam( new Camera(POS, FOV, ASPECT, Z_NEAR, Z_FAR, mInput->getAccel()) ),
    mCelFactory( CelestialFactory::getInstance() ),
    dPlanet(nullptr) {

        for (int i=1; i<argc; ++i) {
            mArgv.push_back( std::string(argv[i]) );
        }

        mInited = init();
        unsigned int time_1 = SDL_GetTicks();
        std::cerr << "Init time: " << time_1 - mTime_0 << " ms\n";

}


Game::~Game() {

    for (auto i : mModels) {
        delete i.second;
    }

    for (auto i : mShaders) {
        delete i.second;
    }

    delete mCam;
    delete mAsset;
    delete mGen;
    delete mEvent;
    delete mWindow;
    delete mCelFactory;

}


bool Game::isInited() {
    return mInited;
}


void Game::run() {

    float deltaTime = 0.0f;
    float lastFrame = 0.0f;     // time until last measurement
    float currentFrame = 0.0f;  // total time
    unsigned int cycleCounter = 0;
    
    while (mInput->handleInput(mEvent)) {

        // update timing
        currentFrame = SDL_GetTicks();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;


        mWindow->clear();


        // draw models
        mModels["sun"]->draw(mShaders["sunShader"], mCam);
        // dPlanet_->draw(shaders_["planetShader"], cam_);
        // dPlanet->update(deltaTime);

        for (unsigned int i=0; i<NUM_PLANETS; ++i) {
           mModels[std::to_string(i)]->draw(mShaders["planetShader"], mCam);
           mModels[std::to_string(i)]->update(deltaTime);
        }
        // mModels["cube"]->draw(mShaders["cubeShader"], mCam);


        glDepthFunc(GL_LEQUAL);
        mModels["skybox"]->draw(mShaders["skyboxShader"], mCam);
        glDepthFunc(GL_LESS);

        
        // update FPS counter
        if (cycleCounter % 10 == 0) {
            mWindow->updateFPSCounter(deltaTime);
            cycleCounter = 0;
            // TODO: http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-11-2d-text/
        }

        mCam->updateView(deltaTime);

        // render present
        mWindow->swapBuffers();
        ++cycleCounter;

    }

}


bool Game::init() {

    // init shaders
    // std::cout << mArgv.size() << std::endl;
    std::string path;
    // if (mArgv.size() > 0 && mArgv[0] == "120") {
    //     path = DIR_120;
    // } else {
    //     path = DIR_330;
    // }
    path = DIR_120;
    const std::string dir = path;
    mShaders.insert( {"sunShader", new Shader(dir, SUN_FS, SUN_VS)} );
    mShaders.insert( {"planetShader", new Shader(dir, PLANET_FS, PLANET_VS)} );
    mShaders.insert( {"skyboxShader", new Shader(dir, SKYBOX_FS, SKYBOX_VS)} ); 
    mShaders.insert( {"cubeShader", new Shader(dir, CUBE_FS, CUBE_VS)} );

    // init models

    
    // TODO: sun texture bug: 
    // if not loaded, all shaders using textures dont work
    // check Mesh, shaders
    float sunR = 1.0f;
    if (mArgv.size() > 0) {
        sunR = std::stod(mArgv[0]);
    }
    mModels.insert( {"sun", mCelFactory->getCelestial(sun, sunR, nullptr,
                                                      mAsset->loadTexture(EARTH_TEX, true))} );

    // utility::printModelStats("sun", mModels);



    VertexVec planetV;
    IndexVec planetI;
    float rad = 1.0f;
    mGen->genShape(planetV, planetI, rad, 20, 20);

    for (unsigned int i=0; i<NUM_PLANETS; ++i) {

        mModels.insert( {std::to_string(i), 
                         mCelFactory->getCelestial(planet, 1.0f, mModels["sun"])} );

    }
    // std::cout << "***planet***" 
    //           << "\n  vertices: " << planetV.size() 
    //           << "\n  indices: " << planetI.size() 
    //           << "\n  size in bytes: " << sizeof( *(mModels["0"]) ) 
    //           << std::endl;
    
    // utility::printModelStats("0", mModels, std::string("planet"));

    // debug planet
    dPlanet = new Planet(planetV,
                         planetI,
                         glm::vec3(4.0f, 0.0f, 0.0f),
                         glm::vec3(0.0f),
                         glm::vec3(1.0f),
                         0);

    glm::vec3 vel(1.0f, 0.0f, 1.0f);
    dPlanet->setOrbitalVelocity(vel);
    dPlanet->setParent(mModels["1"]);

    // skybox
    VertexVec skyV;
    mGen->genCube(skyV);
    MeshVec m;
    utility::genMeshes(m, 6, skyV);
    glm::vec3 pos(0.0f);
    std::vector<unsigned int> texV;
    mAsset->loadCubeMap(SPACEBOX, texV);
    mModels.insert( {"skybox", new Skybox(m, pos, texV)} );
    // unsigned int tex = mAsset->loadCubeMap(SPACEBOX);
    // mModels.insert( {"skybox", new Skybox(skyV, tex)} );

    // cube
    // VertexVec cubeV;
    // mGen->genCube(cubeV);
    // mModels.insert( {"cube", new Model(cubeV, 
    //                                    glm::vec3(0.0f, 0.0f, 0.0f),
    //                                    glm::vec3(0.0f),
    //                                    glm::vec3(1.0f),
    //                                    mAsset->loadTexture(WALL_TEX))} );


    return true;
}
