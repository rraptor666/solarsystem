#ifndef ASSET_HH
#define ASSET_HH

#include "datastructures.hh"
#include "stb_image.h"
#include "utility.hh"
#include <fstream>
#include <GL/glew.h>
#include <iostream>
#include <string>
#include <SDL2/SDL.h>
#include <vector>


class Asset {

public:

    Asset();
    ~Asset();

    unsigned int loadTexture(const std::string &path, bool verticalFlip=true);
    unsigned int loadCubeMap(const std::vector<std::string> &paths,
                             bool addAlpha=false);
    
    void loadCubeMap(const std::vector<std::string> &paths, std::vector<unsigned int> &texVec);

};

#endif // ASSET_HH
