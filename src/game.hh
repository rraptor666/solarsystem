#ifndef GAME_HH
#define GAME_HH

#include "asset.hh"
#include "camera.hh"
#include "celestialfactory.hh"
#include "datastructures.hh"
#include "input.hh"
#include "model.hh"
#include "planet.hh"
#include "shader.hh"
#include "shapegenerator.hh"
#include "skybox.hh"
#include "sun.hh"
#include "window.hh"

#include <sstream>
#include <unordered_map>



class Game {

public:

    Game(int argc, char** argv);
    ~Game();

    bool isInited();
    void run();


private:

    bool init();
    
    bool mInited;
    unsigned int mTime_0;

    Window* mWindow;
    SDL_Event* mEvent;

    ShapeGenerator* mGen;
    Asset* mAsset;
    Input* mInput;
    Camera* mCam;
    CelestialFactory* mCelFactory;

    ShaderMap mShaders;
    ModelMap mModels;

    Planet* dPlanet;

    std::vector<std::string> mArgv;

};

#endif // GAME_HH
