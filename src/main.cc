#include "game.hh"

// TODO: Model class loads meshes with Asset (assimp/tinyobjloader)
// TODO: Mesh::initIndexBuffer


// TODO: model statistics/debug print after init( VAO etc yes/no, vertex count...)

// TODO: mesh with init options: exlude normals/texCoord etc!!!!


// BUG: something wrong with (120?) textures, test sun.fs texture2D
// without "result" -> fixed?


// TODO: reimplement genVerticesWithoutIndices using genVertices and genIndices:
// push(/insert?) vertex to genVertices according to indices

int main(int argc, char** argv) {

    Game gm(argc, argv);
    if (!gm.isInited()) {
        std::cerr << "Initialization failed.\n";
        return 0;
    }
    gm.run();

    return 0;
}
