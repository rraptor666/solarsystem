#ifndef CONSTANTS_HH
#define CONSTANTS_HH

#include <glm/glm.hpp>
#include <string>
#include <vector>
#include <unordered_map>

// window
const std::string W_TITLE = "Solarsystem 3D";
const unsigned int W_WIDTH = 800;
const unsigned int W_HEIGHT = 600;

// camera
const glm::vec3 POS = glm::vec3(0,0,-3);
const float FOV = 70.0f;                                // field of vision
const float ASPECT = float(W_WIDTH)/float(W_HEIGHT);    // viewport dimensions
const float Z_NEAR = 0.1f;                              // distance to closer zy clip plane
const float Z_FAR = 1000.0f;                            // distance to farther zy clip plane

// shaders
const std::string PLANET_FS = "planet.fs";
const std::string PLANET_VS = "planet.vs";
const std::string SUN_FS = "sun.fs";
const std::string SUN_VS = "sun.vs";
const std::string TRI_FS = "triangle.fs";
const std::string TRI_VS = "triangle.vs";
const std::string SKYBOX_FS = "skybox.fs";
const std::string SKYBOX_VS = "skybox.vs";
const std::string CUBE_FS = "cube.fs";
const std::string CUBE_VS = "cube.vs";


const std::string PLANET_FS_120 = "planet_120.fs";
const std::string PLANET_VS_120 = "planet_120.vs";
const std::string SUN_FS_120 = "sun_120.fs";
const std::string SUN_VS_120 = "sun_120.vs";
const std::string SKYBOX_FS_120 = "skybox_120.fs";
const std::string SKYBOX_VS_120 = "skybox_120.vs";

const std::string DIR_120 = "./res/shaders/120/";
const std::string DIR_330 = "./res/shaders/330/";


// textures
const std::string SUN_TEX = "./res/textures/sun_copy.jpg";
const std::string WALL_LINE_TEX = "./res/textures/wall_line.png";
const std::string WALL_TEX = "./res/textures/wall.jpg";
const std::string EARTH_TEX = "./res/textures/earth_2k.jpg";

const std::vector<std::string> LAKEBOX = {

    // "./res/textures/lakecube/back.jpg",
    // "./res/textures/lakecube/front.jpg",
    // "./res/textures/lakecube/right.jpg",
    // "./res/textures/lakecube/left.jpg",
    // "./res/textures/lakecube/bottom.jpg",
    // "./res/textures/lakecube/top.jpg",

    "./res/textures/lakecube0/back.jpg",
    "./res/textures/lakecube0/front.jpg",
    "./res/textures/lakecube0/right.jpg",
    "./res/textures/lakecube0/left.jpg",
    "./res/textures/lakecube0/bottom.jpg",
    "./res/textures/lakecube0/top.jpg",

};

// TODO: needs special loading options
const std::vector<std::string> SPACEBOX = {

    // "./res/textures/spacecube/back.png",
    // "./res/textures/spacecube/front.png",
    // "./res/textures/spacecube/right.png",
    // "./res/textures/spacecube/left.png",
    // "./res/textures/spacecube/bottom.png",
    // "./res/textures/spacecube/top.png",

    "./res/textures/spacecube/back.jpg",
    "./res/textures/spacecube/front.jpg",
    "./res/textures/spacecube/right.jpg",
    "./res/textures/spacecube/left.jpg",
    "./res/textures/spacecube/bottom.jpg",
    "./res/textures/spacecube/top.jpg",
};

// TODO: needs special loading options
const std::vector<std::string> SPACEBOX2 = {

    "./res/textures/spacecube2/back.png",
    "./res/textures/spacecube2/front.png",
    "./res/textures/spacecube2/right.png",
    "./res/textures/spacecube2/left.png",
    "./res/textures/spacecube2/bottom.png",
    "./res/textures/spacecube2/top.png",

};


// rate of sphere's radius to vertices( or indices) for generated
// sphere to look smooth and not made of triangles
const std::unordered_map<float, unsigned int> RAD_VERTICES_RATE = {
    {1.0f, 26},
    {2.0f, 30},
    {3.0f, 30},
    {4.0f, 46},
    {5.0f, 50},

};


// in-game settings
const unsigned int NUM_PLANETS = 10;
enum CelType {sun, planet, moon, asteroid, comet};

#endif // CONSTANTS_HH
