#include "utility.hh"

namespace utility {


std::vector<std::string> split(std::string s,
                               char separator,
                               bool removeSpaces) {

    std::vector<std::string> vec;

    std::string::size_type begin = 0;
    std::string::size_type end = 0;
    std::string line = "";

    while(end != std::string::npos) {

        end = s.find(separator, begin);
        line = s.substr(begin, end-begin);

        if (line != "" || !removeSpaces) {
            vec.push_back(line);
        }
        begin = end + 1;
    }

    return vec;
}


#include <iostream>
// TODO: what if meshCount > v.size()?
void genMeshes(MeshVec &m, const unsigned int meshCount, VertexVec &v) {

    if (meshCount == 0) {
        return;
    }

    unsigned int increment = v.size()/meshCount;
    unsigned int begin = 0;
    unsigned int end = increment;

    for (unsigned int i=0; i<meshCount+1; ++i) {

        VertexVec tmp;
        for (unsigned int j=begin; j<end; ++j) {
            
            if (j > v.size()-1) {
                break;
            }
            tmp.push_back( v[j] );

        }
        if (tmp.size() > 0) {
            m.push_back( new Mesh(tmp) );
        } else {
            break;
        }
        begin = end;
        end += increment;

        // if (end > v.size()) {
        //     end = v.size();
        // } 

        // std::cout << "vertices in the mesh: " << m[i]->getVertexCount() << std::endl;
    }

    // if (end < meshCount-1) {

    // }


}

void printModelStats(const std::string &objName, ModelMap &models, std::string name) {

    auto iter = models.find(objName);
    if (iter == models.end()) {
        std::cerr << "No model "
                  << objName
                  << " found.\n";
    
        return;
    }

    Stat* s = iter->second->getStat();
    s->byteSize += sizeof( *models[objName] );

    std::cout << "***"; 
    if (name != "") {
        std::cout << name; 
    } else {
        std::cout << objName;
    }
    std::cout << "***"
              << "\n  vertices: " << s->vSize 
              << "\n  indices: " << s->iSize 
              << "\n  size in bytes: " << s->byteSize  //sizeof( *(mModels["0"]) ) 
              << std::endl;
}


} // utility