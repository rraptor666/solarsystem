#ifndef CELESTIALFACTORY_HH
#define CELESTIALFACTORY_HH

// #include "moon.hh"
#include "constants.hh"
#include "model.hh"
#include "planet.hh"
#include "shapegenerator.hh"
#include "sun.hh"


class CelestialFactory {

public:
    static CelestialFactory* getInstance();

    Model* getCelestial(CelType type, VertexVec &v, IndexVec &i, 
                        Model* parent=nullptr, unsigned int tex=0);

    Model* getCelestial(CelType type, float radius=1.0f, 
                        Model* parent=nullptr, unsigned int tex=0);

    unsigned int getNumCelestials();



private:
    CelestialFactory();

    static CelestialFactory* mInstance;
    unsigned int mNumCels;


};



#endif