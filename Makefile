
OBJS = src/*.cc

CC = g++

LINKER_FLAGS = -lSDL2 -lGLEW -lGL

COMPILER_FLAGS = -std=c++11 -Wall

OBJ_NAME = solarsys

all : $(OBJS) ; $(CC) $(COMPILER_FLAGS) $(LINKER_FLAGS) $(OBJS) -o $(OBJ_NAME) 
