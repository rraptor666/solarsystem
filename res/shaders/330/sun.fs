#version 330 core

in vec3 FragPos;
in vec2 TexCoord;
in vec3 Normal;

out vec4 FragColor;

uniform sampler2D texCoord0;
uniform vec3 viewPos;

vec3 lightPos = vec3(1.2, 1.0, 2.0);
vec3 lightColor = vec3(0.8, 0.7, 0.02);
vec3 objectColor = vec3(10.0, 2.0, 0.0);


void main() {

    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lightColor;

    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);

    float diff = max(dot(norm, lightDir), 0.002);
    vec3 diffuse = diff * lightColor;


    vec3 result = (ambient + diffuse) * objectColor;
    // FragColor = vec4(result, 1.0);
    FragColor = texture(texCoord0, TexCoord);

}
