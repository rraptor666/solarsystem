#version 330 core
out vec4 FragColor;

in vec3 FragPos;
in vec2 texCoord0;

uniform sampler2D sampler;
//uniform vec3 viewPos;

void main()
{
/*
    FragColor = vec4(vec3(1.0, 1.0, 1.0), 1.0);
    FragColor *= texture(sampler, TexCoord);
*/
    gl_FragColor = texture2D(sampler, texCoord0);
}
