#version 120
varying vec4 FragColor;

varying vec3 FragPos;
varying vec2 texCoord0;

uniform sampler2D sampler;
//uniform vec3 viewPos;

void main()
{
/*
    FragColor = vec4(vec3(1.0, 1.0, 1.0), 1.0);
    FragColor *= texture(sampler, TexCoord);
*/
    gl_FragColor = texture2D(sampler, texCoord0);
}
