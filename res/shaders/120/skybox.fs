#version 120

varying vec2 texCoord0;

uniform sampler2D skybox;

void main()
{
    gl_FragColor = texture2D(skybox, texCoord0);
}