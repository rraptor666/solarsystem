#version 120

varying vec3 FragPos;
varying vec2 TexCoord;
varying vec3 Normal;

uniform sampler2D texCoord0;
uniform vec3 viewPos;

vec3 lightPos = vec3(1.2, 1.0, 2.0);
vec3 lightColor = vec3(0.8, 0.7, 0.02);
vec3 objectColor = vec3(10.0, 2.0, 0.0);

#define PI 3.141592653589793238462643383279

void main() {

    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lightColor;

    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);

    float diff = max(dot(norm, lightDir), 0.002);
    vec3 diffuse = diff * lightColor;


    vec3 result = (ambient + diffuse) * objectColor;
    // gl_FragColor = texture2D(texCoord0, TexCoord) * vec4(result, 1.0);
    
    // test
    // vec2 tc = TexCoord;
    // tc.x = (PI + atan(TexCoord.y, TexCoord.x)) / (2 * PI); // calculate angle and map it to 0..1
    // gl_FragColor = texture2D(texCoord0, tc) * vec4(1.0, 1.0, 1.0, 1.0);
    
    gl_FragColor = texture2D(texCoord0, TexCoord) * vec4(1.0, 1.0, 1.0, 1.0);

}
