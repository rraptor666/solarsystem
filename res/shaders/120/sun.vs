#version 120

attribute vec3 position;
attribute vec2 texCoord;
attribute vec3 normal;

varying vec3 FragPos;
varying vec2 TexCoord;
varying vec3 Normal;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

void main() {

    FragPos = vec3(model * vec4(position, 1.0));
    // gl_Position = projection * view * model * vec4(position, 1.0);
    gl_Position = projection * view * vec4(FragPos, 1.0);

    Normal = normal;
    TexCoord = vec2(texCoord.x, texCoord.y);

}
