#version 120

varying vec3 FragPos;
varying vec2 TexCoord;
varying vec3 Normal;

// varying vec4 FragColor;

uniform sampler2D texCoord0;
uniform vec3 viewPos;

vec3 lightPos = vec3(1.2, 1.0, 2.0);
vec3 lightColor = vec3(0.8, 0.7, 0.02);
vec3 objectColor = vec3(0.08, 0.7, 0.15);
//vec3 objectColor = vec3(1.0, 1.0, 1.0);

void main() {

    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lightColor;

    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);

    float diff = max(dot(norm, lightDir), 0.002);
    vec3 diffuse = diff * lightColor;


    // vec3 result = (ambient + diffuse) * objectColor;
    vec3 result = objectColor;
    gl_FragColor = vec4(result, 1.0);
    //FragColor *= texture(texCoord0, TexCoord);


}
